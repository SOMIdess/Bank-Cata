package com.cata.Bank;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.cata.config.ApplicationConfig;
import com.cata.exception.InsufficientBalanceException;
import com.cata.model.Account;
import com.cata.model.Customer;

import com.cata.service.AccountService;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class AccountTest {

	@Autowired
	private AccountService accountService;

	@org.junit.Before
	public void setup() {

	}

	@Test
	public void withdrawsTest() throws InsufficientBalanceException {
		Customer customer = new Customer("Dupond", "DURAND", "1 Rue de la gaite 75000 Paris");
		Account account = accountService.save(new Account(1000d, customer));
		accountService.withdraws(account.getId(), 50d);
		account= accountService.getAccount(account.getId());
		assertEquals(950d, account.getBalance(), 0);
		assertEquals(1, accountService.getAccountOperations(account.getId()).size());
	}

	@Test(expected = InsufficientBalanceException.class)
	public void illegalWithdrawsTest() throws InsufficientBalanceException {
		Customer customer = new Customer("Dupond", "DURAND", "1 Rue de la gaite 75000 Paris");
		Account account = accountService.save(new Account(1000d, customer));
		accountService.withdraws(account.getId(), 2000d);
	}

	@Test
	public void depositTest() throws InsufficientBalanceException {
		Customer customer = new Customer("Dupond", "DURAND", "1 Rue de la gaite 75000 Paris");
		Account account = accountService.save(new Account(1000d, customer));
		accountService.makeAdeposit(account.getId(), 50d);
		account= accountService.getAccount(account.getId());
		assertEquals(1050d, account.getBalance(), 0);
		assertEquals(1, accountService.getAccountOperations(account.getId()).size());
	}

	@Test(expected = java.lang.AssertionError.class)
	public void negativeDepositTest() throws InsufficientBalanceException {
		Customer customer = new Customer("Dupond", "DURAND", "1 Rue de la gaite 75000 Paris");
		Account account = accountService.save(new Account(1000d, customer));
		accountService.makeAdeposit(account.getId(), -50d);
	}

}

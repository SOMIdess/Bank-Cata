package com.cata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Customer {
    
	@Id
	@GeneratedValue
	private long id;
	
	@Column(name="SHORT_NAME")
	private String shortName;
	
	@Column(name="LONG_NAME")
	private String longName;
	
	@Column(name="ADRESS")
	private String adress;

	public Customer(String shortName, String longName, String adress) {
		this.shortName = shortName;
		this.longName = longName;
		this.adress = adress;
	}
  
	public Customer() {
		super();
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}

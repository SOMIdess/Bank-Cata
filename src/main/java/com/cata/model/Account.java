package com.cata.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Account {
    
	@Id
	@GeneratedValue
	private long id;
	
	@Column(name="BALANCE")
	private double balance;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	private Customer customer;
	
	@OneToMany(mappedBy="account",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Operation> operations;

	public Account() {
		super();
	}

	public Account(double balance, Customer customer) {
		super();
		this.balance = balance;
		this.customer = customer;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public boolean equals(Account account) {
		return getId() == account.getId() && 
				getBalance()==account.getBalance()&&
				getCustomer().equals(account.getCustomer());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj instanceof Account) {
			return equals((Account) obj);
		}
		return false;
	}
}

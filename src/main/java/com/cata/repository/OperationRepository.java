package com.cata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cata.model.Account;
import com.cata.model.Operation;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
	
	@Query("SELECT o FROM Operation AS o where o.account = :account") 
	List<Operation> getAccountOperations(@Param("account")Account account);

}

package com.cata.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cata.exception.InsufficientBalanceException;
import com.cata.model.Account;
import com.cata.repository.AccountRepository;
import com.cata.repository.OperationRepository;
import com.cata.model.Operation;



@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private OperationRepository operationRepository;
	
	public Account save(Account account){
		return accountRepository.save(account);
	}
	
	public void makeAdeposit(long accountId, double amount) {
		assert amount > 0 : "The amount to deposit should be positive";
		Account account = accountRepository.findOne(accountId);
		account.setBalance(account.getBalance() + amount);
		operationRepository.save(new Operation(new Date(),amount,account));
		accountRepository.save(account);
	}
    
	public void withdraws(long accountId, double amount) throws InsufficientBalanceException {
		Account account = accountRepository.findOne(accountId);
		if (account.getBalance() - amount < 0)
			throw new InsufficientBalanceException();
		account.setBalance(account.getBalance() - amount);
		operationRepository.save(new Operation(new Date(), -amount,account));
		accountRepository.save(account);
	}

	public List<Operation> getAccountOperations(long accountId) {
		Account account = accountRepository.findOne(accountId);
		return operationRepository.getAccountOperations(account);
	}

	public Account getAccount(long id) {
		return accountRepository.findOne(id);
	}
}
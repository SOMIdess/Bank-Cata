package com.cata.service;

import java.util.List;

import com.cata.exception.InsufficientBalanceException;
import com.cata.model.Account;
import com.cata.model.Operation;


public interface AccountService {
	
	Account getAccount(long id);
	Account save(Account account);
	void makeAdeposit(long accountId, double amount);
	void withdraws(long accountId, double amount) throws InsufficientBalanceException;
	List<Operation> getAccountOperations(long accountId);
}
